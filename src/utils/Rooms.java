package utils;

/**
 * This is a container for all created rooms.
 */
public enum Rooms {

    /**
     * 
     */
    TUTORIAL,

    /**
     * First room.
     */
    ROOM1,

    /**
     * Second room.
     */
    ROOM2,

    /**
     * Third room.
     */
    ROOM3,

    /**
     * Boos room.
     */
    BOSS_ROOM;
}

